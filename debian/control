Source: coq-gappa
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Julien Puydt <jpuydt@debian.org>
Section: math
Priority: optional
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-ocaml,
               dh-coq,
               gappa,
               libcoq-flocq,
               libcoq-core-ocaml-dev,
               libcoq-stdlib,
               ocaml-findlib
Vcs-Browser: https://salsa.debian.org/ocaml-team/coq-gappa
Vcs-Git: https://salsa.debian.org/ocaml-team/coq-gappa.git
Homepage: https://gappa.gitlabpages.inria.fr

Package: libcoq-gappa
Architecture: any
Depends: gappa, ${coq:Depends}, ${misc:Depends}
Provides: ${coq:Provides}
Description: Coq tactic to use Gappa for floating-point goals
 This package provides a Coq tactic to discharge goals about
 floating-point arithmetic and round-off errors to Gappa.
 .
 Gappa is a prover for numerical properties.
 .
 Coq is a proof assistant for higher-order logic.
